﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusinessWebsite.App_Code
{
    public class Result
    {
        public bool Ok;

        public Result(bool ok)
        {
            Ok = ok;
        }
    }
}