﻿namespace BusinessWebsite.Controllers
{
    public class Result
    {
        public bool Ok;
        public int ID;

        public Result(bool ok,int id)
        {
            Ok = ok;
            ID = id;
        }

        public Result(bool ok)
        {
            Ok = ok;
        }
    }
}