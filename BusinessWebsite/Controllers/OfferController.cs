﻿using BusinessWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BusinessWebsite.Controllers
{
    public class OfferController : Controller
    {
        BusinessDBEntities db = new BusinessDBEntities();

        // GET: Order
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllOffers()
        {
            var offers = db.Offers.ToList().Select(x => new Offer(x)).ToList();

            return Json(offers, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddOffer(String OfferName, String OfferDescription, HttpPostedFileBase Image, bool IsActive, DateTime Date, int BusinessTypeID)
        {
            var offer = new Offer(OfferName, OfferDescription, IsActive, Date, BusinessTypeID);
            if (Image != null && Image.ContentLength > 0)
            {
                String filePath = "~/Uploads/" + Image.FileName;
                Image.SaveAs(Server.MapPath(filePath));
                String imageUrl = "/Uploads/" + Image.FileName;
                offer.ImageUrl = imageUrl;
            }

            db.Offers.Add(offer);
            db.SaveChanges();

            return Json(new Result(true, offer.OfferID), JsonRequestBehavior.AllowGet);

        }

        public ActionResult UpdateOffer(String OfferName, String OfferDescription, HttpPostedFileBase Image, bool IsActive, DateTime Date, int BusinessTypeID)
        {
            var offer = new Offer(OfferName, OfferDescription, IsActive, Date, BusinessTypeID);
            if (Image != null && Image.ContentLength > 0)
            {
                String filePath = "~/Uploads/" + Image.FileName;
                Image.SaveAs(Server.MapPath(filePath));
                String imageUrl = "/Uploads/" + Image.FileName;
                offer.ImageUrl = imageUrl;
            }

            db.Offers.Add(offer);
            db.SaveChanges();

            return Json(new Result(true, offer.OfferID), JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetOffer(int OfferID)
        {
            var o = db.Offers.Find(OfferID);

            var offer = new Offer(o);

            return Json(offer, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteOffer(int OfferID)
        {
            var offer = db.Offers.Find(OfferID);
            var result = new Result(false);

            if (offer != null)
            {
                db.Offers.Remove(offer);
                db.SaveChanges();
                result.Ok = true;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}