﻿using BusinessWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessWebsite;

namespace BusinessWebsite.Controllers
{
    public class ComputerController : Controller
    {
        BusinessDBEntities db = new BusinessDBEntities();

        // GET: Computer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllComputers()
        {
            var computers = db.Computers.ToList().Select(x => new Computer(x,x.ComputerProperties.ToList())).ToList();
            
            return Json(computers, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddComputer(String ComputerName,int Ram , int Hard, int Graphic , int AccountID,HttpPostedFileBase Image,String Description,String Cpu)
        {
            var computer = new Computer(AccountID, Description, Cpu, ComputerName, Ram, Hard, Graphic);

            if(Image!=null && Image.ContentLength > 0)
            {
                String filePath = "~/Uploads/" + Image.FileName;
                Image.SaveAs(Server.MapPath(filePath));
                String imageUrl = "/Uploads/" + Image.FileName;
                computer.ImageUrl = imageUrl;
            }

            db.Computers.Add(computer);
            db.SaveChanges();

            return Json(new Result(true,computer.ComputerID),JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateComputer(int ComputerID,String ComputerName, int Ram, int Hard, int Graphic, int AccountID, HttpPostedFileBase Image, String Description, String Cpu)
        {
            var computer = db.Computers.Find(ComputerID);

            computer.AccountID = AccountID;
            computer.Description = Description;
            computer.Cpu = Cpu;
            computer.ComputerName = ComputerName;
            computer.Ram = Ram;
            computer.Hard = Hard;
            computer.Graphic = Graphic;

            if (Image != null && Image.ContentLength > 0)
            {
                String filePath = "~/Uploads/" + Image.FileName;
                Image.SaveAs(Server.MapPath(filePath));
                String imageUrl = "/Uploads/" + Image.FileName;
                computer.ImageUrl = imageUrl;
            }

            db.SaveChanges();

            return Json(new Result(true), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetComputer(int ComputerID)
        {
            var c = db.Computers.Find(ComputerID);

            var computer = new Computer(c,c.ComputerProperties.ToList());

            return Json(computer, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteComputer(int ComputerID)
        {
            var c = db.Computers.Find(ComputerID);

            db.Computers.Remove(c);
            db.SaveChanges();

            return Json(new Result(true), JsonRequestBehavior.AllowGet);
        }
    }


}