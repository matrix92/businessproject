﻿using BusinessWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BusinessWebsite.Controllers
{
    public class AccountController : Controller
    {
        BusinessDBEntities db = new BusinessDBEntities();

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllAccount()
        {
            var accounts = db.Accounts.ToList().Select(x => new Account(x)).ToList();

            return Json(accounts, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddAccount(int AccountTypeID, String Name, String Address, String Location, String Email, long Phone, int BusinessTypeID,HttpPostedFileBase Image)
        {
            var account = new Account(AccountTypeID, Name, Address, Location, Email, Phone, BusinessTypeID);

            if (Image != null && Image.ContentLength > 0)
            {
                String filePath = "~/Uploads/" + Image.FileName;
                Image.SaveAs(Server.MapPath(filePath));
                String imageUrl = "/Uploads/" + Image.FileName;
                account.ImageUrl = imageUrl;
            }

            db.Accounts.Add(account);
            db.SaveChanges();

            return Json(new Result(true, account.AccountID), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAccount(int AccountTypeID, String Name, String Address, String Location, String Email, long Phone, int BusinessTypeID, HttpPostedFileBase Image,int AccountID)
        {
            var account = db.Accounts.Find(AccountID);

            var result = new Result(false);

            if (account != null)
            {
                account.AccountTypeID = AccountTypeID;
                account.Address = Address;
                account.BusinessTypeID = BusinessTypeID;
                account.Email = Email;
                account.Location = Location;
                account.Name = Name;
                account.Phone = Phone;

                if (Image != null && Image.ContentLength > 0)
                {
                    String filePath = "~/Uploads/" + Image.FileName;
                    Image.SaveAs(Server.MapPath(filePath));
                    String imageUrl = "/Uploads/" + Image.FileName;
                    account.ImageUrl = imageUrl;
                }

                db.SaveChanges();

                result.Ok = true;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteAccount(int AccountID)
        {
            var account = db.Accounts.Find(AccountID);
            var result = new Result(false);

            if (account != null)
            {
                db.Accounts.Remove(account);
                db.SaveChanges();
                result.Ok = true;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAccount(int AccountID)
        {
            var a = db.Accounts.Find(AccountID);

            var account = new Account(a);

            return Json(account, JsonRequestBehavior.AllowGet);
        }
    }
}