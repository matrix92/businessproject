﻿using BusinessWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BusinessWebsite.Controllers
{
    public class BusinessTypeController : Controller
    {
        BusinessDBEntities db = new BusinessDBEntities();

        // GET: BusinessType
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllBusinessTypes()
        {
            var businessTypes = db.BusinessTypes.ToList().Select(x => new BusinessType(x)).ToList();

            return Json(businessTypes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddBusinessType(String TypeName)
        {
            var businessType = new BusinessType(TypeName);

            db.BusinessTypes.Add(businessType);

            return Json(new Result(true, businessType.BusinessTypeID), JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteBusinessType(int BusinessTypeID)
        {
            var businessType = db.BusinessTypes.Find(BusinessTypeID);

            if (businessType != null)
            {
                db.BusinessTypes.Remove(businessType);
                return Json(new Result(true), JsonRequestBehavior.AllowGet);
            }

            return Json(new Result(false), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateBusinessType(int BusinessTypeID,String TypeName)
        {
            var businessType = db.BusinessTypes.Find(BusinessTypeID);
            var result = new Result(false);

            if (businessType != null)
            {
                businessType.TypeName = TypeName;
                db.SaveChanges();
                result.Ok = true;
            } 

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}