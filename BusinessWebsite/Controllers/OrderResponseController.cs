﻿using BusinessWebsite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BusinessWebsite.Controllers
{
    public class OrderResponseController : Controller
    {
        BusinessDBEntities db = new BusinessDBEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddResponse(int AccountID,int OrderID,bool Response)
        {
            var response = new OrderRespons(OrderID, AccountID, Response);

            db.OrderResponses.Add(response);
            db.SaveChanges();

            return Json(new Result(true, response.ResponseID));
        }

        public ActionResult GetOrderResponses(int OrderID)
        {
            var responses = db.OrderResponses.Where(x => x.OrderID == OrderID).ToList().Select(x => new OrderRespons(x)).ToList();

            return Json(responses, JsonRequestBehavior.AllowGet);
        }
    }
}