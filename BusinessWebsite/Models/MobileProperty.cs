//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BusinessWebsite.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MobileProperty
    {
        public MobileProperty()
        {

        }

        public MobileProperty(MobileProperty mp)
        {
            PropertyID = mp.PropertyID;
            MobileID = mp.MobileID;
            Property = mp.Property;
            Value = mp.Value;

        }
        public int PropertyID { get; set; }
        public int MobileID { get; set; }
        public string Property { get; set; }
        public string Value { get; set; }
    
        public virtual Mobile Mobile { get; set; }
    }
}
