//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BusinessWebsite.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductBusinessType
    {
        public int ProdBusTypeID { get; set; }
        public int ProductID { get; set; }
        public int BusinessTypeID { get; set; }
    
        public virtual BusinessType BusinessType { get; set; }
        public virtual Product Product { get; set; }
    }
}
